// eslint-disable-next-line react/prop-types
function Button({ children }) {

  return (
    <button type="button">{ children }</button>
  );
}

export default Button;
